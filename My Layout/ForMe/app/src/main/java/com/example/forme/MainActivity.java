package com.example.forme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button flasd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Handler h = new Handler();
        h.postDelayed(new Runnable(){
            public void run(){
                startFlasd();
            }
        },1000); //sleep for 1 second then flash the button


    }

    public void startFlasd(){
        flasd=(Button)findViewById(R.id.button);
        Animation mAnimation = new AlphaAnimation(1,0);
        mAnimation.setDuration(1000);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);
        flasd.startAnimation(mAnimation);
    }

    public void gopro(View view) {
        Intent intent = new Intent(MainActivity.this,profile.class);
        startActivity(intent);
    }

}